Les commandes souvent utilisé c'est :

- git pull : pour récupérer le contenu de la branch

- git status : pour voir les fichiers en cours de modification

- git add . : pour tout séléctionner
 ou git add <nom de fichier spécifique> : pour séléctionner un fichier précisemment

- git commit -m "" : pour mettre un message au commit de tes modifications

- git push : pour envoyer le ou les commits en attente de push

- git fetch : pour récupérer des branches depuis un ou plusieurs autres dépôts